<?php
namespace app\index\controller;
use app\common\controller\HomeBase;
use app\common\logic\Common as LogicCommon;



class Article extends  HomeBase
{
	
	/**
	 * 文章逻辑
	 */
	
	private static $commonLogic = null;

	
	public function _initialize()
	{
		parent::_initialize();
		
		self::$commonLogic = get_sington_object('commonLogic', LogicCommon::class);
	}
	
   public function index($id){
   	
   	if($id>0){
   		
   	self::$commonLogic->setDataValue('article',['id'=>$id], 'view', array('exp','view+1'));
   	
   	$info = self::$commonLogic->getDataInfo('article',['id'=>$id]);
   	
   
   	
   	$this->assign('info',$info);
   	
   	}else{
   		
   		$this->error('非法操作', 'index/index');
   		
   	}
   	return $this->fetch();
   	
   }
   
   public function artlist($id){
   
   	if($id>0){
   		 
   		
   
   		$artlist = self::$commonLogic->getDataList('article',['tid'=>$id]);
   
   		$this->assign('id',$id);
   
   		$this->assign('artlist',$artlist);
   
   	}else{
   		 
   		$this->error('非法操作', 'index/index');
   		 
   	}
   	return $this->fetch();
   
   }
   
}
